<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use app\models\Soccer;

$this->title = 'soccer';
$this->params['breadcrumbs'][] = $this->title;
 
$query = Soccer::find();

$provider = new ActiveDataProvider([
        'query' => $query,
       'pagination' => [
            'pageSize' => 2,
        ],
    ]);
    
    
    /*
    $provider = new ArrayDataProvider([
       'allModels' => $model->getAll()
    ]);
    */
?>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <?=
   
   GridView::widget([
       'dataProvider' => $provider,
       'columns' => [
            'id',
            'name',
            'team',
           
       ]
    ])
   
    
    ?>   
   

    <code><?= __FILE__ ?></code>
</div>