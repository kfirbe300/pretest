<?php

use yii\db\Migration;

/**
 * Handles the creation of table `soccer`.
 */
class m180621_083412_create_soccer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('soccer', [
          'id' => $this->primaryKey(),
          'name' => $this->string(),
          'team' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('soccer');
    }
}
